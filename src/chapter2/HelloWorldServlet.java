package chapter2;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/HelloWorld")
public class HelloWorldServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		try {
			System.out.println("begin hello, world! " + getCurrentTime());

			req.setAttribute("message",  "hello, world!");

			RequestDispatcher dispatcher =
					req.getRequestDispatcher("/HelloWorldServlet.jsp");
			dispatcher.forward(req, res);
		} finally {
			System.out.println("end hello, world! " + getCurrentTime());
		}
	}

	private String getCurrentTime() {
		return new SimpleDateFormat("HH:mm:ss").format(new Date());
	}
}
